<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Domain;

use Throwable;

/**
 * Throw exception implementing this interface in TransactionManagerInterface::wrapInTransaction() in order not to close DB connection by ORM after rollback.
 * Intended to use by logic rollbacks
 */
interface LogicTerminationInterface extends Throwable
{
}
