<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Domain;

use Throwable;

interface TransactionManagerInterface
{
    /**
     * Executes a function in a transaction.
     * Everything in a closure either gets committed or rolled back.
     * Any exception inside callback causes transaction rollback in database.
     *
     * @throws LogicTerminationInterface database connection is not closed on transaction rollback
     * @throws Throwable database connection is closed on transaction rollback
     */
    public function wrapInTransaction(callable $callback): void;
}
