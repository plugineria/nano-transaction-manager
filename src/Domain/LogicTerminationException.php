<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Domain;

use LogicException;

class LogicTerminationException extends LogicException implements LogicTerminationInterface
{
}
