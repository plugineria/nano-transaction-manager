<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure;

use FriendsOfDdd\TransactionManager\Domain\TransactionManagerInterface;
use Throwable;

class MockedTransactionManager implements TransactionManagerInterface
{
    public ?Throwable $expectedException = null;

    public function wrapInTransaction(callable $callback): void
    {
        if (null !== $this->expectedException) {
            throw $this->expectedException;
        }

        $callback();
    }
}
