<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Flusher;

use FriendsOfDdd\TransactionManager\Application\FlusherInterface;

final class LazyFlusherDecorator implements FlusherInterface
{
    private int $bufferSize = 0;
    private bool $isRoot = true;

    public function __construct(
        private FlusherInterface $originalFlusher,
        /**
         * If 0 (default): will flush once after all inline callbacks complete.
         * If not 0: will flush after the amount of callbacks completed.
         */
        private int $maxBufferSize = 0
    ) {
    }

    /**
     * Flush when all inline calls are done or after reaching every $maxBufferSize of callbacks
     */
    public function flushOnComplete(callable $callback): void
    {
        $isRoot = $this->isRoot;
        $this->isRoot = false;

        try {
            $callback();

            $this->bufferSize++;

            if ($this->isCleanBuffer($isRoot)) {
                $this->originalFlusher->flush();
            }
        } finally {
            if ($this->isCleanBuffer($isRoot)) {
                $this->bufferSize = 0;
            }

            $this->isRoot = $isRoot;
        }
    }

    /**
     * Force flush without any conditions. Clears buffer of completed callbacks if not empty.
     */
    public function flush(): void
    {
        $this->originalFlusher->flush();
        $this->bufferSize = 0;
    }

    private function isCleanBuffer(bool $isRoot): bool
    {
        return $isRoot || (0 !== $this->maxBufferSize && $this->bufferSize >= $this->maxBufferSize);
    }
}
