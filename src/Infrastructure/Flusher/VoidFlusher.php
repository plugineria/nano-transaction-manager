<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Flusher;

use FriendsOfDdd\TransactionManager\Application\FlusherInterface;

class VoidFlusher implements FlusherInterface
{
    public function flushOnComplete(callable $callback): void
    {
        $callback();
    }

    public function flush(): void
    {
    }
}
