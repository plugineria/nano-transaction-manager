<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Application;

interface FlusherInterface
{
    /**
     * Flush(sync local ORM cache to database) or queue flush on callback completes
     */
    public function flushOnComplete(callable $callback): void;

    /**
     * Force flush (sync local ORM cache to database) immediately
     */
    public function flush(): void;
}
