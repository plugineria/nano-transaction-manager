<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Tests\Unit\Infrastructure\Flusher;

use FriendsOfDdd\TransactionManager\Infrastructure\Flusher\VoidFlusher;
use PHPUnit\Framework\TestCase;

class VoidOrmSessionFlusherTest extends TestCase
{
    public function testFlushOnCompleteBreaksNothing(): void
    {
        $flusher = new VoidFlusher();
        $updatedValue = false;

        $flusher->flushOnComplete(
            static function () use (&$updatedValue) {
                $updatedValue = true;
            }
        );

        self::assertTrue($updatedValue);
    }

    public function testFlushBreaksNothing(): void
    {
        $flusher = new VoidFlusher();

        $flusher->flush();

        self::assertTrue(true);
    }
}
