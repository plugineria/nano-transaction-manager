<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Tests\Unit\Infrastructure\Flusher;

use ArrayObject;
use FriendsOfDdd\TransactionManager\Application\FlusherInterface;
use FriendsOfDdd\TransactionManager\Infrastructure\Flusher\LazyFlusherDecorator;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class LazyFlusherDecoratorTest extends TestCase
{
    private LazyFlusherDecorator $flusher;
    private ArrayObject $debugInfo;
    private FlusherInterface $mockedFlusher;

    protected function setUp(): void
    {
        $this->debugInfo = new ArrayObject();
        $this->mockedFlusher = new class ($this->debugInfo) implements FlusherInterface {
            public function __construct(private ArrayObject $debugInfo)
            {
            }

            public function flushOnComplete(callable $callback): void
            {
                $callback();
                $this->debugInfo->append('FLUSH_ON_COMPLETE');
            }

            public function flush(): void
            {
                $this->debugInfo->append('DO_FLUSH');
            }
        };

        $this->flusher = new LazyFlusherDecorator(
            $this->mockedFlusher
        );
    }

    public function testOnlyOuterFlushInvoked(): void
    {
        $this->flusher->flushOnComplete(function () {
            $this->debugInfo->append('LEVEL1');

            $this->flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.1');

                $this->flusher->flushOnComplete(function () {
                    $this->debugInfo->append('LEVEL2.1.1');
                });
            });

            $this->flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.2');
            });
        });

        self::assertEquals(
            [
                'LEVEL1',
                'LEVEL2.1',
                'LEVEL2.1.1',
                'LEVEL2.2',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testFlushesEveryTimeWhenExplicitFlushInvoked(): void
    {
        $this->flusher->flushOnComplete(function () {
            $this->debugInfo->append('LEVEL1');

            $this->flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.1');

                $this->flusher->flush();
                $this->flusher->flush();
                $this->flusher->flush();

                $this->flusher->flushOnComplete(function () {
                    $this->debugInfo->append('LEVEL2.1.1');

                    $this->flusher->flush();
                });
            });

            $this->flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.2');
            });
        });

        self::assertEquals(
            [
                'LEVEL1',
                'LEVEL2.1',
                'DO_FLUSH',
                'DO_FLUSH',
                'DO_FLUSH',
                'LEVEL2.1.1',
                'DO_FLUSH',
                'LEVEL2.2',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testFlushInvokedEachSecondCallAccordingToMaxBufferSize(): void
    {
        $flusher = new LazyFlusherDecorator(
            $this->mockedFlusher,
            maxBufferSize: 2
        );

        $flusher->flushOnComplete(function () use ($flusher) {
            $this->debugInfo->append('LEVEL1');

            $flusher->flushOnComplete(function () use ($flusher) {
                $this->debugInfo->append('LEVEL2.1');

                $flusher->flushOnComplete(function () use ($flusher) {
                    $this->debugInfo->append('LEVEL2.1.1');
                });
            });

            $this->flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.2');
            });
        });

        self::assertEquals(
            [
                'LEVEL1',
                'LEVEL2.1',
                'LEVEL2.1.1',
                'DO_FLUSH',
                'LEVEL2.2',
                'DO_FLUSH',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testFlushInvokedEachSecondCallAndEveryExplicitFlushAccordingToMaxBufferSize(): void
    {
        $flusher = new LazyFlusherDecorator(
            $this->mockedFlusher,
            maxBufferSize: 2
        );

        $flusher->flushOnComplete(function () use ($flusher) {
            $this->debugInfo->append('LEVEL1 BEGIN');

            $flusher->flushOnComplete(function () use ($flusher) {
                $this->debugInfo->append('LEVEL2.1 BEGIN');

                $flusher->flush();
                $flusher->flush();
                $flusher->flush();

                $flusher->flushOnComplete(function () use ($flusher) {
                    $this->debugInfo->append('LEVEL2.1.1 BEGIN');

                    $flusher->flush();

                    $this->debugInfo->append('LEVEL2.1.1 END');
                });

                $this->debugInfo->append('LEVEL2.1 END');
            });

            $flusher->flushOnComplete(function () use ($flusher) {
                $this->debugInfo->append('LEVEL2.2');
            });

            $flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.3');
            });

            $flusher->flushOnComplete(function () {
                $this->debugInfo->append('LEVEL2.4');
            });

            $this->debugInfo->append('LEVEL1 END');
        });

        self::assertEquals(
            [
                'LEVEL1 BEGIN',
                'LEVEL2.1 BEGIN',
                'DO_FLUSH',
                'DO_FLUSH',
                'DO_FLUSH',
                'LEVEL2.1.1 BEGIN',
                'DO_FLUSH',
                'LEVEL2.1.1 END',
                'LEVEL2.1 END',
                'DO_FLUSH',
                'LEVEL2.2',
                'LEVEL2.3',
                'DO_FLUSH',
                'LEVEL2.4',
                'LEVEL1 END',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testExceptionInCallbackDoesNotDoFlushAndAllowsToFlushNextCall(): void
    {
        $exception = new RuntimeException('Something wrong');

        try {
            $this->flusher->flushOnComplete(function () use ($exception) {
                $this->debugInfo->append('FIRST_CALLBACK');
                throw $exception;
            });
        } catch (RuntimeException $e) {
            self::assertSame($exception, $e);
        }

        $this->flusher->flushOnComplete(function () {
            $this->debugInfo->append('SECOND_CALLBACK');
        });

        self::assertEquals(
            [
                'FIRST_CALLBACK',
                'SECOND_CALLBACK',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testExceptionInFlushAllowsToFlushNextTime(): void
    {
        $flusher = new LazyFlusherDecorator(
            new class ($this->debugInfo) implements FlusherInterface {
                private int $index = 0;

                public function __construct(private ArrayObject $debugInfo)
                {
                }

                public function flushOnComplete(callable $callback): void
                {
                    $callback();

                    $this->flush();
                }

                public function flush(): void
                {
                    if (0 === $this->index++) {
                        $this->debugInfo->append('DO_FLUSH_WITH_FAILURE');
                        throw new RuntimeException('Something wrong');
                    } else {
                        $this->debugInfo->append("DO_FLUSH");
                    }
                }
            },
        );

        try {
            $flusher->flushOnComplete(function () {
                $this->debugInfo->append('FIRST_CALLBACK');
            });
        } catch (RuntimeException) {
            self::assertTrue(true);
        }

        $this->flusher->flushOnComplete(function () {
            $this->debugInfo->append('SECOND_CALLBACK');
        });

        self::assertEquals(
            [
                'FIRST_CALLBACK',
                'DO_FLUSH_WITH_FAILURE',
                'SECOND_CALLBACK',
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }

    public function testFlushWithNoCallbackAsRootJustFlushes(): void
    {
        $flusher = new LazyFlusherDecorator(
            new class ($this->debugInfo) implements FlusherInterface {
                private int $index = 0;

                public function __construct(private ArrayObject $debugInfo)
                {
                }

                public function flushOnComplete(callable $callback): void
                {
                    $callback();

                    $this->flush();
                }

                public function flush(): void
                {
                    $this->debugInfo->append("DO_FLUSH");
                }
            },
        );

        $flusher->flush();

        self::assertEquals(
            [
                'DO_FLUSH',
            ],
            $this->debugInfo->getArrayCopy()
        );
    }
}
